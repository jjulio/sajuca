﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SajucaData.Models
{
    public partial class SajucaContext : DbContext
    {
        public SajucaContext()
        {
        }

        public SajucaContext(DbContextOptions<SajucaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AdditionOrderDetail> AdditionOrderDetails { get; set; }
        public virtual DbSet<Addition> Additions { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Table> Tables { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Zone> Zones { get; set; }

        // Unable to generate entity type for table 'public.categories_additions'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Server=localhost;Database=sajucadb;Uid=postgres;Pwd=Ut1w04wZ;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdditionOrderDetail>(entity =>
            {
                entity.ToTable("addition_order_details");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.IdAddition)
                    .HasColumnName("id_addition")
                    .ForNpgsqlHasComment("identificador registro tabla additions");

                entity.Property(e => e.IdOrderDetail)
                    .HasColumnName("id_order_detail")
                    .ForNpgsqlHasComment("identificador registro tabla order_details");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.HasOne(d => d.IdAdditionNavigation)
                    .WithMany(p => p.AdditionOrderDetails)
                    .HasForeignKey(d => d.IdAddition)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("addition_order_details_additions_fk");

                entity.HasOne(d => d.IdOrderDetailNavigation)
                    .WithMany(p => p.AdditionOrderDetails)
                    .HasForeignKey(d => d.IdOrderDetail)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("addition_order_details_order_details_fk");
            });

            modelBuilder.Entity<Addition>(entity =>
            {
                entity.ToTable("additions");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("character varying(500)")
                    .ForNpgsqlHasComment("descripción adición");

                entity.Property(e => e.Enable)
                    .HasColumnName("enable")
                    .ForNpgsqlHasComment("registro activo o inactivo");

                entity.Property(e => e.ImagePath)
                    .HasColumnName("image_path")
                    .HasColumnType("character varying(255)")
                    .ForNpgsqlHasComment("nombre producto");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("character varying(100)")
                    .ForNpgsqlHasComment("nombre adición");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .ForNpgsqlHasComment("precio adición");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("categories");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.Enable)
                    .HasColumnName("enable")
                    .ForNpgsqlHasComment("registro activo o inactivo");

                entity.Property(e => e.ImagePath)
                    .HasColumnName("image_path")
                    .HasColumnType("character varying(255)")
                    .ForNpgsqlHasComment("ruta imagen categoría");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("character varying(100)")
                    .ForNpgsqlHasComment("nombre categoría");
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.ToTable("order_details");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .ForNpgsqlHasComment("identificador registro tabla orders");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .ForNpgsqlHasComment("identificador registro tabla products");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.OrderComment)
                    .HasColumnName("order_comment")
                    .HasColumnType("character varying(80)")
                    .ForNpgsqlHasComment("comentarios del pedido");

                entity.HasOne(d => d.IdOrderNavigation)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.IdOrder)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("order_details_orders_fk");

                entity.HasOne(d => d.IdProductNavigation)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.IdProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("order_details_products_fk");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("orders");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CardTotal)
                    .HasColumnName("card_total")
                    .ForNpgsqlHasComment("valor pago con tarjeta");

                entity.Property(e => e.CashTotal)
                    .HasColumnName("cash_total")
                    .ForNpgsqlHasComment("valor pago efectivo");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .ForNpgsqlHasComment("valor descuento");

                entity.Property(e => e.IdState)
                    .HasColumnName("id_state")
                    .ForNpgsqlHasComment("identificador registro tabla states");

                entity.Property(e => e.IdTable)
                    .HasColumnName("id_table")
                    .ForNpgsqlHasComment("identificador registro tabla tables");

                entity.Property(e => e.IdUser)
                    .HasColumnName("id_user")
                    .ForNpgsqlHasComment("identificador registro usuario");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.OrderDate)
                    .HasColumnName("order_date")
                    .ForNpgsqlHasComment("fecha compra");

                entity.Property(e => e.OrderTotal)
                    .HasColumnName("order_total")
                    .ForNpgsqlHasComment("total compra");

                entity.Property(e => e.Tip)
                    .HasColumnName("tip")
                    .ForNpgsqlHasComment("valor propina");

                entity.Property(e => e.Voucher)
                    .HasColumnName("voucher")
                    .HasColumnType("character varying(200)")
                    .ForNpgsqlHasComment("número voucher pago tarjeta");

                entity.HasOne(d => d.IdStateNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.IdState)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orders_states_fk");

                entity.HasOne(d => d.IdTableNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.IdTable)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orders_tables_fk");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orders_users_fk");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("products");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("character varying(500)")
                    .ForNpgsqlHasComment("descripción producto");

                entity.Property(e => e.Enable)
                    .HasColumnName("enable")
                    .ForNpgsqlHasComment("registro activo o inactivo");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .ForNpgsqlHasComment("identificador registro tabla categories");

                entity.Property(e => e.ImagePath)
                    .HasColumnName("image_path")
                    .HasColumnType("character varying(255)")
                    .ForNpgsqlHasComment("ruta imagen producto");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("character varying(100)")
                    .ForNpgsqlHasComment("nombre producto");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .ForNpgsqlHasComment("precio producto");

                entity.HasOne(d => d.IdCategoryNavigation)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.IdCategory)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("products_categories_fk");
            });

            modelBuilder.Entity<Profile>(entity =>
            {
                entity.ToTable("profiles");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.Enable)
                    .HasColumnName("enable")
                    .ForNpgsqlHasComment("registro activo o inactivo");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("character varying(50)")
                    .ForNpgsqlHasComment("nombre perfil");
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.ToTable("states");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.Enable)
                    .HasColumnName("enable")
                    .ForNpgsqlHasComment("activo o inactivo");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("character varying(20)")
                    .ForNpgsqlHasComment("nombre estado");
            });

            modelBuilder.Entity<Table>(entity =>
            {
                entity.ToTable("tables");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.Enable)
                    .HasColumnName("enable")
                    .ForNpgsqlHasComment("activo o inactivo");

                entity.Property(e => e.IdZone)
                    .HasColumnName("id_zone")
                    .ForNpgsqlHasComment("identificador zona");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("character varying(20)")
                    .ForNpgsqlHasComment("nombre mesa");

                entity.HasOne(d => d.IdZoneNavigation)
                    .WithMany(p => p.Tables)
                    .HasForeignKey(d => d.IdZone)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tables_fk");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.Username)
                    .HasName("users_un")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.Enable)
                    .HasColumnName("enable")
                    .ForNpgsqlHasComment("registro activo o inactivo");

                entity.Property(e => e.IdProfile)
                    .HasColumnName("id_profile")
                    .ForNpgsqlHasComment("identificador registro tabla profile");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasColumnType("character varying(50)")
                    .ForNpgsqlHasComment("contraseña usuario");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasColumnType("character varying(30)")
                    .ForNpgsqlHasComment("nombre usuario");

                entity.HasOne(d => d.IdProfileNavigation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.IdProfile)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("users_profiles_fk");
            });

            modelBuilder.Entity<Zone>(entity =>
            {
                entity.ToTable("zones");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('zones_id_seq1'::regclass)")
                    .ForNpgsqlHasComment("identificador registro");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creation_date")
                    .ForNpgsqlHasComment("fecha creación");

                entity.Property(e => e.Enable)
                    .HasColumnName("enable")
                    .ForNpgsqlHasComment("registro activo o inactivo");

                entity.Property(e => e.ModificationDate)
                    .HasColumnName("modification_date")
                    .ForNpgsqlHasComment("fecha modificación");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("character varying(15)")
                    .ForNpgsqlHasComment("nombre zona");
            });

            modelBuilder.HasSequence("zones_id_seq");

            modelBuilder.HasSequence<int>("zones_id_seq1");
        }
    }
}
