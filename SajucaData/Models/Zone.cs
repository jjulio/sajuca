﻿using System;
using System.Collections.Generic;

namespace SajucaData.Models
{
    public partial class Zone
    {
        public Zone()
        {
            Tables = new HashSet<Table>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enable { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }

        public ICollection<Table> Tables { get; set; }
    }
}
