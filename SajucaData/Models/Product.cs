﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SajucaData.Models
{
    public partial class Product
    {
        public Product()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int IdCategory { get; set; }
        public double Price { get; set; }
        public bool Enable { get; set; }
        public string ImagePath { get; set; }
        [NotMapped]
        public string ImageName { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        [NotMapped]
        public string ContentFile { get; set; }
        public Category IdCategoryNavigation { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
        [NotMapped]
        public List<Addition> Additions { get; set; }
    }
}
