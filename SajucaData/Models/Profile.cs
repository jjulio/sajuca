﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SajucaData.Models
{
    public partial class Profile
    {
        public Profile()
        {
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        [StringLength(maximumLength: 50, ErrorMessage = "El campo {0} debe tener máximo {1} caracteres.")]
        public string Name { get; set; }
        public bool Enable { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
