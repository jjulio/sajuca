﻿using System;
using System.Collections.Generic;

namespace SajucaData.Models
{
    public partial class User
    {
        public User()
        {
            Orders = new HashSet<Order>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int IdProfile { get; set; }
        public bool Enable { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }

        public Profile IdProfileNavigation { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
