﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SajucaData.Models
{
    public partial class Addition
    {
        public Addition()
        {
            AdditionOrderDetails = new HashSet<AdditionOrderDetail>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public bool Enable { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }

        public ICollection<AdditionOrderDetail> AdditionOrderDetails { get; set; }
    }
}
