﻿using System;
using System.Collections.Generic;

namespace SajucaData.Models
{
    public partial class AdditionOrderDetail
    {
        public int Id { get; set; }
        public int IdOrderDetail { get; set; }
        public int IdAddition { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }

        public Addition IdAdditionNavigation { get; set; }
        public OrderDetail IdOrderDetailNavigation { get; set; }
    }
}
