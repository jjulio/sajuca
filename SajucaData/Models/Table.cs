﻿using System;
using System.Collections.Generic;

namespace SajucaData.Models
{
    public partial class Table
    {
        public Table()
        {
            Orders = new HashSet<Order>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enable { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public int IdZone { get; set; }

        public Zone IdZoneNavigation { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
