﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SajucaData.Models
{
    public partial class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }
        [StringLength(maximumLength: 100, ErrorMessage = "El campo {0} debe tener máximo {1} caracteres.")]
        public string Name { get; set; }
        public bool Enable { get; set; }
        public string ImagePath { get; set; }
        [NotMapped]
        public string ImageName { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        [NotMapped]
        public string ContentFile { get; set; }
        public ICollection<Product> Products { get; set; }
        [NotMapped]
        public List<Addition> Additions { get; set; }
    }
}
