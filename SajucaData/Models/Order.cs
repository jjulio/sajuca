﻿using System;
using System.Collections.Generic;

namespace SajucaData.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdState { get; set; }
        public double OrderTotal { get; set; }
        public DateTime OrderDate { get; set; }
        public double Tip { get; set; }
        public int IdTable { get; set; }
        public string Voucher { get; set; }
        public double CardTotal { get; set; }
        public double CashTotal { get; set; }
        public int Discount { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }

        public State IdStateNavigation { get; set; }
        public Table IdTableNavigation { get; set; }
        public User IdUserNavigation { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
