﻿using System;
using System.Collections.Generic;

namespace SajucaData.Models
{
    public partial class OrderDetail
    {
        public OrderDetail()
        {
            AdditionOrderDetails = new HashSet<AdditionOrderDetail>();
        }

        public int Id { get; set; }
        public int IdOrder { get; set; }
        public int IdProduct { get; set; }
        public string OrderComment { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }

        public Order IdOrderNavigation { get; set; }
        public Product IdProductNavigation { get; set; }
        public ICollection<AdditionOrderDetail> AdditionOrderDetails { get; set; }
    }
}
