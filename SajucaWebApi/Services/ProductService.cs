﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SajucaData.Models;
using SajucaUtility;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SajucaWebApi.Services
{
    public class ProductService : IProductService
    {
        private readonly IConfiguration configuration;
        private readonly SajucaContext context;
        private readonly IImageHelper imageHeper;

        public ProductService(IConfiguration configuration, SajucaContext context, IImageHelper imageHelper)
        {
            this.configuration = configuration;
            this.context = context;
            this.imageHeper = imageHelper;
        }

        public IEnumerable<Product> GetProducts()
        {
            return this.context.Products.ToList();
        }

        public int UpdateProduct(Product product)
        {
            this.context.Update(product);
            return this.context.SaveChanges();
        }

        async Task<int> IProductService.CreateProduct(Product product, string section, string key)
        {
            product.ImagePath = await this.imageHeper.UploadImage(product.ContentFile, section, key, product.ImageName);
            this.context.Add(product);
            return this.context.SaveChanges();
        }

        Product IProductService.GetProductById(int id)
        {
            // return this.context.Products.FirstOrDefault(x => x.Id == id);

            return this.context.Products
                 .Where(p => p.Id == id)
                 .Include("IdCategoryNavigation")
                 .FirstOrDefault();


        }

        IEnumerable<Product> IProductService.GetProducts()
        {
            return this.context.Products.ToList();
        }

        async Task<int> IProductService.UpdateProduct(Product product, string section, string key)
        {
            if (File.Exists(product.ImagePath))
            {
                File.Delete(product.ImagePath);
            }

            product.ImagePath = await this.imageHeper.UploadImage(product.ContentFile, section, key, product.ImageName);
            this.context.Update(product);
            return this.context.SaveChanges();
        }
    }
}
