﻿using SajucaData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SajucaWebApi.Services
{
    public interface ICategoryService
    {
        Task<int> CreateCategory(Category category, string section, string key);
        IEnumerable<Category> GetCategories();
        Category GetCategoryById(int id);
        Task<int> UpdateCategory(Category category, string section, string key);
    }
}
