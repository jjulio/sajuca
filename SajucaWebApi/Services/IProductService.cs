﻿using Microsoft.Extensions.Configuration;
using SajucaData.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SajucaWebApi.Services
{
    public interface IProductService
    {
        Task<int> CreateProduct(Product product, string section, string key);
        IEnumerable<Product> GetProducts();
        Product GetProductById(int id);
        Task<int> UpdateProduct(Product product, string section, string key);

    }
}
