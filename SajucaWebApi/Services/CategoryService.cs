﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SajucaData.Models;
using SajucaUtility;

namespace SajucaWebApi.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IConfiguration configuration;
        private readonly SajucaContext context;
        private readonly IImageHelper imageHeper;

        public CategoryService(IConfiguration configuration, SajucaContext context, IImageHelper imageHelper)
        {
            this.configuration = configuration;
            this.context = context;
            this.imageHeper = imageHelper;
        }

        async Task<int> ICategoryService.CreateCategory(Category category, string section, string key)
        {
            category.ImagePath = await this.imageHeper.UploadImage(category.ContentFile, section, key, category.ImageName);
            this.context.Add(category);
            return this.context.SaveChanges();
        }

        IEnumerable<Category> ICategoryService.GetCategories()
        {
            return this.context.Categories.Include("Additions").ToList();
        }

        Category ICategoryService.GetCategoryById(int id)
        {
            return this.context.Categories.FirstOrDefault(x => x.Id == id);
        }

        async Task<int> ICategoryService.UpdateCategory(Category category, string section, string key)
        {
            if (File.Exists(category.ImagePath))
            {
                File.Delete(category.ImagePath);
            }

            category.ImagePath = await this.imageHeper.UploadImage(category.ContentFile, section, key, category.ImageName);
            this.context.Update(category);
            return this.context.SaveChanges();
        }
    }
}
