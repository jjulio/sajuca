﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SajucaData.Models;

namespace SajucaWebApi.Controllers
{
    [Route("api/Profiles")]
    public class ProfileController : Controller
    {
        private readonly SajucaContext context;

        public ProfileController(SajucaContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Profile> GetProfiles()
        {
            return this.context.Profiles.ToList();
        }

        [HttpGet("{id}", Name = "createProfile")]
        public IActionResult GetProfileById(int id)
        {
            var profile = this.context.Profiles.FirstOrDefault(x => x.Id == id);

            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }

        [HttpPost]
        public IActionResult CreateProfile([FromBody] Profile profile)
        {
            if (ModelState.IsValid)
            {
                this.context.Add(profile);
                this.context.SaveChanges();
                return new CreatedAtRouteResult("createProfile", new { id = profile.Id }, profile);
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateProfile([FromBody] Profile profile, int id)
        {
            if (profile.Id != id)
            {
                return BadRequest();
            }

            if (ModelState.IsValid)
            {
                this.context.Update(profile);
                this.context.SaveChanges();
                return Ok();
            }

            return BadRequest(ModelState);
        }
    }
}