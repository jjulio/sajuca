﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SajucaData.Models;
using SajucaUtility;
using SajucaWebApi.Services;


namespace SajucaWebApi.Controllers
{
    [Route("api/Categories")]
    public class CategoryController : Controller
    {
        private readonly SajucaContext context;
        private readonly IImageHelper imageHelper;

        public ICategoryService Categoryservice { get; set; }

        public CategoryController(SajucaContext context, IConfiguration configuration, ICategoryService categoryService)
        {
            this.context = context;
            this.imageHelper = new ImageHelper(configuration);
            this.Categoryservice = categoryService;
        }

        [HttpGet]
        public IEnumerable<Category> GetCategories()
        {
            return this.Categoryservice.GetCategories();
        }

        [HttpGet("{id}", Name = "createCategory")]
        public IActionResult GetCategoryById(int id)
        {
            var category = this.Categoryservice.GetCategoryById(id);

            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategoryAsync([FromBody] Category category)
        {
            if (ModelState.IsValid)
            {
                if (await this.Categoryservice.CreateCategory(category, "Appsettings", "ImagePath") == 1)
                {
                    return new CreatedAtRouteResult("createCategory", new { id = category.Id }, category);
                }
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCategoryAsync([FromBody] Category category, int id)
        {
            if (category.Id == id)
            {
                return BadRequest();
            }

            if (ModelState.IsValid)
            {
                if (await this.Categoryservice.UpdateCategory(category, "Appsettings", "ImagePath") == 1)
                {
                    return Ok();
                }
            }

            return BadRequest(ModelState);
        }
    }
}