﻿using Microsoft.AspNetCore.Mvc;
using SajucaData.Models;
using SajucaWebApi.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SajucaWebApi.Controllers
{
    [Route("api/Products")]
    public class ProductController : Controller
    {
        public IProductService ProductService { get; set; }

        public ProductController(IProductService productService)
        {
            this.ProductService = productService;
        }

        [HttpGet]
        public IEnumerable<Product> GetProducts()
        {
            return this.ProductService.GetProducts();
        }

        [HttpGet("{id}", Name = "createProduct")]
        public IActionResult GetProductById(int id)
        {
            var product = this.ProductService.GetProductById(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProductAsync([FromBody] Product product)
        {
            if (ModelState.IsValid)
            {
                if (await this.ProductService.CreateProduct(product, "Appsettings", "ImagePath") == 1)
                {
                    return new CreatedAtRouteResult("createProduct", new { id = product.Id }, product);
                }
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProductAsync([FromBody] Product product, int id)
        {
            if (product.Id != id)
            {
                return BadRequest();
            }

            if (ModelState.IsValid)
            {
                if (await this.ProductService.UpdateProduct(product, "Appsettings", "ImagePath") == 1)
                {
                    return Ok();
                }
            }

            return BadRequest(ModelState);
        }
    }
}