﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SajucaData.Models;

namespace SajucaWebApi.Controllers
{
    [Route("api/Orders")]
    public class OrderController : Controller
    {
        private readonly SajucaContext context;

        public OrderController(SajucaContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Order> GetOrders()
        {
            return this.context.Orders.Include(x=> x.OrderDetails).ToList();
        }

        [HttpGet("{id}", Name = "createOrder")]
        public IActionResult GetOrderById(int id)
        {
            var order = this.context.Orders.Include(x=> x.OrderDetails).FirstOrDefault(x => x.Id == id);

            if (order == null)
            {
                NotFound();
            }

            return Ok(order);
        }

        [HttpPost]
        public IActionResult CreateOrder([FromBody] Order order)
        {
            if (ModelState.IsValid)
            {
                this.context.Add(order);
                this.context.SaveChanges();
                return new CreatedAtRouteResult("createOrder", new { id = order.Id }, order);
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateOrder([FromBody] Order order, int id)
        {
            if (order.Id != id)
            {
                return BadRequest();
            }

            if (ModelState.IsValid)
            {
                this.context.Update(order);
                this.context.SaveChanges();
                return Ok();
            }

            return BadRequest(ModelState);
        }
    }
}