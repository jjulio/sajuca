﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace SajucaUtility
{
    public class ImageHelper : IImageHelper
    {
        private readonly IConfiguration configuration;
        private readonly SajucaSetting sajucaSetting;

        public ImageHelper(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.sajucaSetting = SajucaSetting.GetInstance(configuration);
        }

        Task<string> IImageHelper.UploadImage(string contentFileBase64, string section, string key, string fileName)
        {
            byte[] byteArray = Convert.FromBase64String(contentFileBase64);
            
            if (!CheckIfImageFile(byteArray))
            {
                throw new Exception("El archivo no es una imágen con formato válido");
            }

            if (string.IsNullOrEmpty(fileName))
            {
                throw new Exception("El nombre del archivo es requerido");
            }

            return GetImageAsync(byteArray, section, key, fileName);
        }

        private bool CheckIfImageFile(byte[] fileContent)
        {
            return WriterHelper.GetImageFormat(fileContent) != WriterHelper.ImageFormat.unknown;
        }

        private async Task<string> GetImageAsync(byte[] byteArray, string section, string key, string fileName)
        {
            string imagePath = Path.Combine(this.sajucaSetting.GetConfiguration(section, key), fileName);

            if (File.Exists(imagePath))
            {
                File.Delete(imagePath);
            }

            await File.WriteAllBytesAsync(imagePath, byteArray);
            return imagePath;
        }
    }
}
