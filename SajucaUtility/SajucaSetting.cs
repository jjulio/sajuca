﻿using Microsoft.Extensions.Configuration;
using System;

namespace SajucaUtility
{
    public class SajucaSetting
    {
        private IConfiguration configuration;
        private static readonly SajucaSetting instance = new SajucaSetting();

        private SajucaSetting() { }

        public static SajucaSetting GetInstance(IConfiguration configuration)
        {
            instance.configuration = configuration;
            return instance;
        }

        public string GetConfiguration(string section, string key)
        {
            if (string.IsNullOrEmpty(section))
            {
                throw new Exception("valor invalido para variable section.");
            }

            if (string.IsNullOrEmpty(section))
            {
                throw new Exception("valor invalido para variable key");
            }

            return configuration.GetSection("SajucaSettings").GetSection(section).GetSection(key).Value;
        }
    }
}
