﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SajucaUtility
{
    public interface IImageHelper
    {
        Task<string> UploadImage(string contentFileBase64, string section, string key, string fileName);
    }
}
